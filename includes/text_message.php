<?php
	session_start();
	require("db_connect.php");

	$query=mysqli_query($mysqli,"SELECT * FROM users WHERE text_opt='1'") or die('fail');
	$numrows = mysqli_num_rows($query);

	if($numrows!=0){
		while($row=mysqli_fetch_assoc($query)){
			$user_id=$row['user_id'];
			$cell=$row['cell_number'];
			$carrier=$row['carrier'];
			$savings_balance=$row['savings_balance'];
			$savings_cushion=$row['savings_cushion'];
			$deadline=$row['deadline'];
			$number_of_weeks=$deadline/7;
			$weekly_savings=$savings_cushion/$number_of_weeks;
			$daily_savings=$savings_cushion/$deadline;
			$months=$deadline/30;
			
			if($carrier=="sprint"){
				$carrier="messaging.sprintpcs.com";
			}
			else if($carrier=="t-mobile"){
				$carrier="tmomail.net";
			}
			else if($carrier=="verizon"){
				$carrier="vtext.com";
			}
			else if($carrier=="att"){
				$carrier="txt.att.net";
			}
			else if($carrier=="nextel"){
				$carrier="messaging.nextel.com";
			}
			$to = $cell."@".$carrier;
		    $headers = "From: Financail Forest <financialforest@brettdavidconnolly.com> \r\n";
			$message = "To save $".$savings_cushion." in ".$months." months, you need to save $".round($weekly_savings,2)." each week ($".round($daily_savings,2)." a day). Deposit $".round($weekly_savings,2)." every Friday.";
			mail($to, '', $message, $headers);
			echo("To: ".$to."Headers: ".$headers."Message: ".$message."/n");
			$registerquery=mysqli_query($mysqli,"INSERT INTO notifications (user_id,n_date,notification)VALUES('$user_id',NOW(),'$message')")or die('insert notification: db-fail');
		}
	}

?>
